/*
Copyright 2021 The Xiadat Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

import (
	"context"
	"fmt"
	"time"
)

func main() {
	ctx ,cancel :=context.WithTimeout(context.Background(),time.Millisecond*500)
	go g4(ctx)

	count := 1
	for {
		if count >= 3 {
			fmt.Println("手动取消")
			cancel()
			count = 0
		}
		time.Sleep(time.Second)
		if count>0{
			count++
		}
	}
}

func g4(ctx context.Context)  {
	done := make(chan struct{},1)

	go func() {
		time.Sleep(time.Second*1)
		done <- struct{}{}
	}()

	select {
	case <- ctx.Done():
		fmt.Println("g3 超时取消了")
		return
	case <- done:
		fmt.Println("正常执行完成")
		return
	}

/*
	输出顺序:
   g3 超时取消了
   手动取消
 */
}


func main02() {
	ctx ,cancel :=context.WithTimeout(context.Background(),time.Second*5)
	go g3(ctx)

	count := 1
	for {
		if count >= 3 {
			fmt.Println("手动取消")
			cancel()
			count = 0
		}
		time.Sleep(time.Second)
		if count>0{
			count++
		}
	}
}

func g3(ctx context.Context)  {
	done := make(chan struct{},1)

	go func() {
		time.Sleep(time.Second*3)
		done <- struct{}{}
	}()

	select {
	case <- ctx.Done():
		fmt.Println("g3 超时取消了")
		return
	case <- done:
		fmt.Println("正常执行完成")
		return
	}

/*
	输出顺序：
	手动取消
	g3 超时取消了
 */
}

func main01() {
	ctx, cancel := context.WithCancel(context.Background())
	go g1(ctx)

	count := 1
	for {
		if count >= 3 {
			cancel()
		}
		time.Sleep(time.Second)
		count++
	}
}

func g1(ctx context.Context) {
	go g2(ctx)
	select {
	case <-ctx.Done():
		fmt.Println("g1 取消了")
		return
	}
}

func g2(ctx context.Context) {
	select {
	case <-ctx.Done():
		fmt.Println("g2 取消了")
		return
	}
}
