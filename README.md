# Link

- https://www.flysnow.org/2017/05/12/go-in-action-go-context.html

# Context 

只能传递信号，并不能真正的取消。

## WithCancel

-  cancel：调用cancel()方法只是传递一个取消的信号
```shell script
ctx,cancel :=context.WithCancel(context.Background())
```

## WithTimeout

```shell script
ctx ,cancel :=context.WithTimeout(context.Background(),time.Millisecond*500)
```

## select 

- 没有default会阻塞
```shell script
// 有default 
	select {
	case <- ctx.Done():
	default:
  }

// 没有default
select {
  	case <- ctx.Done():
}
```
